from solfonica.settings import MEDIA_ROOT
from django.conf.urls import patterns, include, url
from django.views.generic import TemplateView

# Uncomment the next two lines to enable the admin:
from django.contrib import admin
admin.autodiscover()

urlpatterns = patterns('',
    # Examples:
    url(r'^$', 'eventos.views.home', name='home'),
    url(r'^forum/', include('djangobb_forum.urls', namespace='djangobb')),
    url(r'^fotos/', include('photologue.urls'), name='fotos'),
    url(r'^calendario/(?P<year>\d\d\d\d)/(?P<month>\d{1,2})/', 'eventos.views.calendarwrapper', name='calendarwrapper'),
    url(r'^calendario/', 'eventos.views.calendarwrapper', name='calendar'),
    url(r'^noticias/', 'eventos.views.news', name='noticias'),
    url(r'^contacto/', TemplateView.as_view(template_name='eventos/contacto.html')),
    url(r'^archivos/', 'eventos.views.files', name='archivos'),
    url(r'^mensajes/', include('messages.urls'), name='mensajes'),
    url(r'^users/', 'eventos.views.members', name='miembros'),
    url(r'^accounts/.*', 'eventos.views.redirect_to_index'),
    url(r'^login/$', 'eventos.views.mylogin', name='entrar'),
    url(r'^logout/$', 'eventos.views.mylogout', name='salir'),
    url(r'media/(?P<path>.*)$', 'django.views.static.serve',
        {'document_root': MEDIA_ROOT, 'show_indexes': True}),
    # url(r'^solfonica/', include('solfonica.foo.urls')),

    # Uncomment the admin/doc line below to enable admin documentation:
    # url(r'^admin/doc/', include('django.contrib.admindocs.urls')),

    # Uncomment the next line to enable the admin:
    url(r'^interfaz/administrativa/', include(admin.site.urls)),
    url(r'^poll/', include('poll.urls')),
)
