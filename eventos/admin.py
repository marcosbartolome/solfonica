from eventos.models import UserProfile, Article, Category, Event, Folder, EventType, Act
from eventos.models import FileType, Topic, Song, File, Instrument, Voice
from django.contrib import admin
from django.contrib.auth.models import User

class CategoryAdmin(admin.ModelAdmin):
    list_display = ['name',]
admin.site.register(Category, CategoryAdmin)

class AssistanceInline(admin.TabularInline):
    model = Event.users.through

class EventAdmin(admin.ModelAdmin):
    list_display = ['title',]
    inlines = (AssistanceInline,)
admin.site.register(Event, EventAdmin)

class UserProfileAdmin(admin.ModelAdmin):
    list_display = ['user',]
    filter_horizontal = ['galleries', 'instruments', ]
admin.site.register(UserProfile, UserProfileAdmin)

class UserAdmin(admin.ModelAdmin):
    inlines = (AssistanceInline,)

admin.site.unregister(User)
admin.site.register(User, UserAdmin)

class FilesInline(admin.TabularInline):
    model = File.folders.through

class FolderAdmin(admin.ModelAdmin):
    list_display = ['name',]
    inlines = (FilesInline,)
admin.site.register(Folder, FolderAdmin)

class FileTypeAdmin(admin.ModelAdmin):
    list_display = ['name', 'extensions',]
admin.site.register(FileType, FileTypeAdmin)

class ArticleAdmin(admin.ModelAdmin):
    list_display = ['title', 'link', 'attachment',]
    filter_horizontal = ['topics', ]
admin.site.register(Article, ArticleAdmin)

class TopicAdmin(admin.ModelAdmin):
    list_display = ['name',]
admin.site.register(Topic, TopicAdmin)

class SongAdmin(admin.ModelAdmin):
    list_display = ['title',]
admin.site.register(Song, SongAdmin)

class FileAdmin(admin.ModelAdmin):
    list_display = ['title',]
    filter_horizontal = ['songs', 'folders', ]
admin.site.register(File, FileAdmin)

class InstrumentAdmin(admin.ModelAdmin):
    list_display = ['name', 'code',]
admin.site.register(Instrument, InstrumentAdmin)

class VoiceAdmin(admin.ModelAdmin):
    list_display = ['name', 'code',]
admin.site.register(Voice, VoiceAdmin)

class EventTypeAdmin(admin.ModelAdmin):
    list_display = ['name', ]
admin.site.register(EventType, EventTypeAdmin)

class ActAdmin(admin.ModelAdmin):
    pass
admin.site.register(Act, ActAdmin)
