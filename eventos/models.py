# encoding: utf-8
from django.db import models
from eventos import signals
from eventos.signals import create_profile
from django.contrib.auth.models import User
from eventos.resizeavatar import ResizedImageField, ResizedArticleImageField
from solfonica.settings import PHOTOLOGUE_DIR
from django.template.defaultfilters import slugify
from autoslug import AutoSlugField
import os

ASSISTANCE_CHOICES = (
        ('confirmed', 'confirmado'),
        ('doubtful', 'dudoso'),
        ('absent', 'ausente'),
        ('unanswered', 'sin respuesta'),
        )

#from south.modelsinspector import add_introspection_rules
#add_introspection_rules([], ["^eventos\.resizeavatar\.fields\.ResizedImageField"])
#add_introspection_rules([], ["^eventos\.resizeavatar\.fields\.ResizedArticleImageField"])

def get_news_path(instance, filename):
    return os.path.join(PHOTOLOGUE_DIR, 'news', filename)

def get_avatar_path(instance, filename):
    return os.path.join(PHOTOLOGUE_DIR, 'avatar', filename)


class Instrument(models.Model):
    name = models.CharField(max_length=300)
    code = models.CharField(max_length=20)

    def __unicode__(self):
        return self.name

    class Meta:
        verbose_name = u'instrumento'
        ordering = ['name',]

class Voice(models.Model):
    name = models.CharField(max_length=300)
    code = models.CharField(max_length=20)

    def __unicode__(self):
        return self.name

    class Meta:
        verbose_name = u'voz'
        verbose_name = u'voces'


class UserProfile(models.Model):
    user = models.ForeignKey(User, unique=True, editable=False, verbose_name=u'usuario')
    telephone = models.DecimalField(max_digits=9, decimal_places=0, null=True, blank=True, verbose_name=u'teléfono')
    address = models.CharField(max_length=300, blank=True, null=True, verbose_name=u'dirección')
    description = models.TextField(null=True, blank=True, verbose_name=u'descripción')
    instruments = models.ManyToManyField(Instrument, blank=True, null=True, verbose_name=u'instrumentos')
    galleries = models.ManyToManyField('photologue.Gallery', null=True, blank=True, verbose_name=u'galerías')
    photos = models.ManyToManyField('photologue.Photo', null=True, blank=True, verbose_name=u'fotos')
    messages = models.ManyToManyField('messages.Message', null=True, blank=True, verbose_name=u'mensajes')
    avatar = ResizedImageField(blank=True, null=True, upload_to=get_avatar_path, verbose_name=u'avatar')
    signals.post_save.connect(create_profile, sender=User)

    def __unicode__(self):
        return self.user.username

    class Meta:
        verbose_name = u'perfil de usuario'
        verbose_name_plural = u'perfiles de usuario'

class Category(models.Model):
    name = models.CharField(max_length=50, verbose_name=u'nombre')
    galleries = models.ManyToManyField('photologue.Gallery', null=True, blank=True, verbose_name=u'galerías')
    photos = models.ManyToManyField('photologue.Photo', null=True, blank=True, verbose_name=u'fotos')
    avatari = models.ManyToManyField('photologue.Photo', related_name='avatari', null=True, blank=True, verbose_name=u'avatar')
    slug = AutoSlugField(populate_from='name')

    def __unicode__(self):
        return self.name

    class Meta:
        verbose_name = u'categoría'


class EventType(models.Model):
    name = models.CharField(max_length=50, verbose_name=u'nombre')
    slug = AutoSlugField(populate_from='name')

    def __unicode__(self):
        return self.name

    class Meta:
        verbose_name = u'tipo de evento'
        verbose_name_plural = u'tipos de evento'

class Event(models.Model):
    author = models.ForeignKey(User, verbose_name=u'autor')
    start = models.DateTimeField(verbose_name=u'inicio')
    end = models.DateTimeField(blank=True, null=True, verbose_name=u'fin')
    title = models.CharField(max_length=200, verbose_name=u'título')
    event_type = models.ForeignKey(EventType, verbose_name=u'tipo de evento')
    address = models.CharField(max_length=500, verbose_name=u'lugar')
    description = models.CharField(max_length=1000, blank=True, null=True, verbose_name=u'descripción')
    is_active = models.BooleanField(default=True, verbose_name=u'está activo')
    users = models.ManyToManyField(User, related_name='events', through='Assistance', verbose_name=u'usuarios')
    
    def __unicode__(self):
        return self.title

    def get_absolute_url(self):
        return "/eventos/%i" % self.id

    class Meta:
        verbose_name = u'evento'


class Assistance(models.Model):
    user = models.ForeignKey(User, verbose_name=u'usuario')
    event = models.ForeignKey(Event, verbose_name=u'evento')
    status = models.CharField(max_length=100, choices=ASSISTANCE_CHOICES, verbose_name=u'estado')

    class Meta:
        verbose_name = u'compromiso de asistencia'
        verbose_name = u'compromisos de asistencia'


class Topic(models.Model):
    name = models.CharField(max_length=100, verbose_name=u'nombre')
    description = models.CharField(max_length=500, blank=True, null=True, verbose_name=u'descripción')
    
    def __unicode__(self):
        return self.name

    class Meta:
        verbose_name = u'tema'


class Article(models.Model):
    title = models.CharField(max_length=200, verbose_name=u'título')
    description = models.TextField(verbose_name=u'descripción')
    link = models.URLField(max_length=500, blank=True, null=True)
    youtube_video = models.URLField(max_length=500, blank=True, null=True, verbose_name=u'vídeo de youtube')
    attachment = models.FileField(upload_to='attachments', blank=True, null=True, verbose_name=u'archivo')
    image = ResizedArticleImageField(upload_to=get_news_path, blank=True, null=True, verbose_name=u'imagen')
    topics = models.ManyToManyField(Topic, verbose_name=u'temas')

    class Meta:
        verbose_name = u'artículo'


class FileType(models.Model):
    name = models.CharField(max_length=300, verbose_name=u'nombre')
    extensions = models.CharField(max_length=300, blank=True, null=True, verbose_name=u'extensiones')

    def __unicode__(self):
        return self.name

    class Meta:
        verbose_name = u'tipo de archivo'
        verbose_name_plural = u'tipos de archivo'


class Folder(models.Model):
    name = models.CharField(max_length=200, verbose_name=u'nombre')

    def __unicode__(self):
        return self.name

    class Meta:
        verbose_name = u'carpeta'
        verbose_name_plural = u'carpetas'
        ordering = ['name']


class Song(models.Model):
    title = models.CharField(max_length=300, verbose_name=u'título')
    instruments = models.ManyToManyField(Instrument, blank=True, null=True, verbose_name=u'instrumentos')

    def __unicode__(self):
        return self.title

    class Meta:
        verbose_name = u'canción'
        verbose_name_plural = u'canciones'
        ordering = ['title']


class File(models.Model):
    title = models.CharField(max_length=300, verbose_name=u'título')
    ffile = models.FileField(upload_to='files', verbose_name=u'archivo')
    ftype = models.ManyToManyField(FileType, verbose_name=u'tipo de archivo')
    instrument = models.ForeignKey(Instrument, blank=True, null=True, verbose_name=u'instrumento')
    Voice = models.ForeignKey(Voice, blank=True, null=True, verbose_name=u'voces')
    songs = models.ManyToManyField(Song, blank=True, null=True, verbose_name=u'canciones')
    folders = models.ManyToManyField(Folder, blank=True, null=True, verbose_name=u'carpetas')

    def __unicode__(self):
        return self.title

    class Meta:
        verbose_name = u'archivo'
        ordering = ['title']

class Act(models.Model):
    day = models.DateField(verbose_name=u'fecha')
    afile = models.FileField(upload_to='actas', verbose_name=u'Archivo')

    def __unicode__(self):
        return 'Acta ' + self.day.strftime('%Y-%m-%d')

    class Meta:
        verbose_name=u'acta'
