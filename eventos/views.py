from eventos.models import Event, Article, File, UserProfile, EventType
from django.shortcuts import render_to_response
from django.http import HttpResponseRedirect
from django.template import RequestContext
from django.contrib.auth import authenticate, login, logout
from django.views.decorators.csrf import csrf_protect
from calendar import monthrange
import datetime

def home(request):
    if request.user.is_authenticated():
        articles = Article.objects.all()[:5]
        d = dict(user=request.user, articles=articles)
        return render_to_response('eventos/home.html', d,
            context_instance=RequestContext(request))
    else:
        d = dict()
        return render_to_response('eventos/home.html', d,
            context_instance=RequestContext(request))

def calendarwrapper(request, year=2013, month=3):
    eventos = Event.objects.filter(start__month=month)
    tipos = EventType.objects.all()
    d = dict(eventos=eventos, user=request.user, year=year, month=month,
            tipos=tipos)
    return render_to_response('eventos/calendar.html', d,
            context_instance=RequestContext(request))

def news(request):
    articles = Article.objects.all()
    d = dict(articles=articles, user=request.user)
    return render_to_response('eventos/news.html', d,
            context_instance=RequestContext(request))

def files(request):
    files = File.objects.all()
    d = dict(files=files, user=request.user)
    return render_to_response('eventos/files.html', d,
            context_instance=RequestContext(request))

def members(request):
    users = UserProfile.objects.all()
    d = dict(users=users, user=request.user)
    return render_to_response('eventos/members.html', d,
            context_instance=RequestContext(request))

def redirect_to_index(request):
    return HttpResponseRedirect('/')

@csrf_protect
def mylogin(request):
  if request.method == 'POST':
    user = authenticate(username=request.POST['username'], password=request.POST['password'])
    if user:
        login(request, user)
        # success
        return HttpResponseRedirect('/')
      
def mylogout(request):
  logout(request)
  return render_to_response('eventos/home.html',
          context_instance=RequestContext(request))


##############################################################################
# snippet event calendar 
##############################################################################

def named_month(month_number):
    """
    Return the name of the month, given the number.
    """
    return datetime.date(1900, month_number, 1).strftime("%B")

def this_month(request):
    """
    Show calendar of events this month.
    """
    today = datetime.now()
    return calendar(request, today.year, today.month)


def calendar(request, year, month, series_id=None):
    """
    Show calendar of events for a given month of a given year.
    ``series_id``
    The event series to show. None shows all event series.

    """

    my_year = int(year)
    my_month = int(month)
    my_calendar_from_month = datetime(my_year, my_month, 1)
    my_calendar_to_month = datetime(my_year, my_month, monthrange(my_year, my_month)[1])

    my_events = Event.objects.filter(date_and_time__gte=my_calendar_from_month).filter(date_and_time__lte=my_calendar_to_month)
    if series_id:
        my_events = my_events.filter(series=series_id)

    # Calculate values for the calendar controls. 1-indexed (Jan = 1)
    my_previous_year = my_year
    my_previous_month = my_month - 1
    if my_previous_month == 0:
        my_previous_year = my_year - 1
        my_previous_month = 12
    my_next_year = my_year
    my_next_month = my_month + 1
    if my_next_month == 13:
        my_next_year = my_year + 1
        my_next_month = 1
    my_year_after_this = my_year + 1
    my_year_before_this = my_year - 1
    return render_to_response("cal_template.html", { 'events_list': my_events,
                                                        'month': my_month,
                                                        'month_name': named_month(my_month),
                                                        'year': my_year,
                                                        'previous_month': my_previous_month,
                                                        'previous_month_name': named_month(my_previous_month),
                                                        'previous_year': my_previous_year,
                                                        'next_month': my_next_month,
                                                        'next_month_name': named_month(my_next_month),
                                                        'next_year': my_next_year,
                                                        'year_before_this': my_year_before_this,
                                                        'year_after_this': my_year_after_this,
    }, context_instance=RequestContext(request))



