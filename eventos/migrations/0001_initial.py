# -*- coding: utf-8 -*-
import datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding model 'Instrument'
        db.create_table('eventos_instrument', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=300)),
            ('code', self.gf('django.db.models.fields.CharField')(max_length=20)),
        ))
        db.send_create_signal('eventos', ['Instrument'])

        # Adding model 'UserProfile'
        db.create_table('eventos_userprofile', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('user', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['auth.User'], unique=True)),
            ('telephone', self.gf('django.db.models.fields.DecimalField')(null=True, max_digits=9, decimal_places=0, blank=True)),
            ('address', self.gf('django.db.models.fields.CharField')(max_length=300, null=True, blank=True)),
            ('description', self.gf('django.db.models.fields.TextField')(null=True, blank=True)),
            ('avatar', self.gf('eventos.resizeavatar.ResizedImageField')(max_length=100, null=True, blank=True)),
        ))
        db.send_create_signal('eventos', ['UserProfile'])

        # Adding M2M table for field galleries on 'UserProfile'
        db.create_table('eventos_userprofile_galleries', (
            ('id', models.AutoField(verbose_name='ID', primary_key=True, auto_created=True)),
            ('userprofile', models.ForeignKey(orm['eventos.userprofile'], null=False)),
            ('gallery', models.ForeignKey(orm['photologue.gallery'], null=False))
        ))
        db.create_unique('eventos_userprofile_galleries', ['userprofile_id', 'gallery_id'])

        # Adding M2M table for field photos on 'UserProfile'
        db.create_table('eventos_userprofile_photos', (
            ('id', models.AutoField(verbose_name='ID', primary_key=True, auto_created=True)),
            ('userprofile', models.ForeignKey(orm['eventos.userprofile'], null=False)),
            ('photo', models.ForeignKey(orm['photologue.photo'], null=False))
        ))
        db.create_unique('eventos_userprofile_photos', ['userprofile_id', 'photo_id'])

        # Adding M2M table for field messages on 'UserProfile'
        db.create_table('eventos_userprofile_messages', (
            ('id', models.AutoField(verbose_name='ID', primary_key=True, auto_created=True)),
            ('userprofile', models.ForeignKey(orm['eventos.userprofile'], null=False)),
            ('message', models.ForeignKey(orm['messages.message'], null=False))
        ))
        db.create_unique('eventos_userprofile_messages', ['userprofile_id', 'message_id'])

        # Adding M2M table for field instruments on 'UserProfile'
        db.create_table('eventos_userprofile_instruments', (
            ('id', models.AutoField(verbose_name='ID', primary_key=True, auto_created=True)),
            ('userprofile', models.ForeignKey(orm['eventos.userprofile'], null=False)),
            ('instrument', models.ForeignKey(orm['eventos.instrument'], null=False))
        ))
        db.create_unique('eventos_userprofile_instruments', ['userprofile_id', 'instrument_id'])

        # Adding model 'Category'
        db.create_table('eventos_category', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=50)),
            ('slug', self.gf('django.db.models.fields.CharField')(max_length=50)),
        ))
        db.send_create_signal('eventos', ['Category'])

        # Adding M2M table for field galleries on 'Category'
        db.create_table('eventos_category_galleries', (
            ('id', models.AutoField(verbose_name='ID', primary_key=True, auto_created=True)),
            ('category', models.ForeignKey(orm['eventos.category'], null=False)),
            ('gallery', models.ForeignKey(orm['photologue.gallery'], null=False))
        ))
        db.create_unique('eventos_category_galleries', ['category_id', 'gallery_id'])

        # Adding M2M table for field photos on 'Category'
        db.create_table('eventos_category_photos', (
            ('id', models.AutoField(verbose_name='ID', primary_key=True, auto_created=True)),
            ('category', models.ForeignKey(orm['eventos.category'], null=False)),
            ('photo', models.ForeignKey(orm['photologue.photo'], null=False))
        ))
        db.create_unique('eventos_category_photos', ['category_id', 'photo_id'])

        # Adding M2M table for field avatari on 'Category'
        db.create_table('eventos_category_avatari', (
            ('id', models.AutoField(verbose_name='ID', primary_key=True, auto_created=True)),
            ('category', models.ForeignKey(orm['eventos.category'], null=False)),
            ('photo', models.ForeignKey(orm['photologue.photo'], null=False))
        ))
        db.create_unique('eventos_category_avatari', ['category_id', 'photo_id'])

        # Adding model 'Event'
        db.create_table('eventos_event', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('author', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['auth.User'])),
            ('start', self.gf('django.db.models.fields.DateTimeField')()),
            ('end', self.gf('django.db.models.fields.DateTimeField')(null=True, blank=True)),
            ('title', self.gf('django.db.models.fields.CharField')(max_length=200)),
            ('description', self.gf('django.db.models.fields.CharField')(max_length=1000, null=True, blank=True)),
            ('is_active', self.gf('django.db.models.fields.BooleanField')(default=True)),
        ))
        db.send_create_signal('eventos', ['Event'])

        # Adding model 'Assistance'
        db.create_table('eventos_assistance', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('user', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['auth.User'])),
            ('event', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['eventos.Event'])),
            ('status', self.gf('django.db.models.fields.CharField')(max_length=100)),
        ))
        db.send_create_signal('eventos', ['Assistance'])

        # Adding model 'Topic'
        db.create_table('eventos_topic', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=100)),
            ('description', self.gf('django.db.models.fields.CharField')(max_length=500, null=True, blank=True)),
        ))
        db.send_create_signal('eventos', ['Topic'])

        # Adding model 'Article'
        db.create_table('eventos_article', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('title', self.gf('django.db.models.fields.CharField')(max_length=200)),
            ('description', self.gf('django.db.models.fields.TextField')()),
            ('link', self.gf('django.db.models.fields.URLField')(max_length=500, null=True, blank=True)),
            ('attachment', self.gf('django.db.models.fields.files.FileField')(max_length=100, null=True, blank=True)),
            ('image', self.gf('eventos.resizeavatar.ResizedImageField')(max_length=100, null=True, blank=True)),
        ))
        db.send_create_signal('eventos', ['Article'])

        # Adding M2M table for field topic on 'Article'
        db.create_table('eventos_article_topic', (
            ('id', models.AutoField(verbose_name='ID', primary_key=True, auto_created=True)),
            ('article', models.ForeignKey(orm['eventos.article'], null=False)),
            ('topic', models.ForeignKey(orm['eventos.topic'], null=False))
        ))
        db.create_unique('eventos_article_topic', ['article_id', 'topic_id'])

        # Adding model 'FileType'
        db.create_table('eventos_filetype', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=300)),
            ('extensions', self.gf('django.db.models.fields.CharField')(max_length=300, null=True, blank=True)),
        ))
        db.send_create_signal('eventos', ['FileType'])

        # Adding model 'Folder'
        db.create_table('eventos_folder', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=200)),
        ))
        db.send_create_signal('eventos', ['Folder'])

        # Adding model 'Song'
        db.create_table('eventos_song', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('title', self.gf('django.db.models.fields.CharField')(max_length=300)),
        ))
        db.send_create_signal('eventos', ['Song'])

        # Adding M2M table for field instruments on 'Song'
        db.create_table('eventos_song_instruments', (
            ('id', models.AutoField(verbose_name='ID', primary_key=True, auto_created=True)),
            ('song', models.ForeignKey(orm['eventos.song'], null=False)),
            ('instrument', models.ForeignKey(orm['eventos.instrument'], null=False))
        ))
        db.create_unique('eventos_song_instruments', ['song_id', 'instrument_id'])

        # Adding model 'File'
        db.create_table('eventos_file', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('title', self.gf('django.db.models.fields.CharField')(max_length=300)),
            ('ffile', self.gf('django.db.models.fields.files.FileField')(max_length=100)),
            ('instrument', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['eventos.Instrument'], null=True, blank=True)),
        ))
        db.send_create_signal('eventos', ['File'])

        # Adding M2M table for field ftype on 'File'
        db.create_table('eventos_file_ftype', (
            ('id', models.AutoField(verbose_name='ID', primary_key=True, auto_created=True)),
            ('file', models.ForeignKey(orm['eventos.file'], null=False)),
            ('filetype', models.ForeignKey(orm['eventos.filetype'], null=False))
        ))
        db.create_unique('eventos_file_ftype', ['file_id', 'filetype_id'])

        # Adding M2M table for field songs on 'File'
        db.create_table('eventos_file_songs', (
            ('id', models.AutoField(verbose_name='ID', primary_key=True, auto_created=True)),
            ('file', models.ForeignKey(orm['eventos.file'], null=False)),
            ('song', models.ForeignKey(orm['eventos.song'], null=False))
        ))
        db.create_unique('eventos_file_songs', ['file_id', 'song_id'])

        # Adding M2M table for field folders on 'File'
        db.create_table('eventos_file_folders', (
            ('id', models.AutoField(verbose_name='ID', primary_key=True, auto_created=True)),
            ('file', models.ForeignKey(orm['eventos.file'], null=False)),
            ('folder', models.ForeignKey(orm['eventos.folder'], null=False))
        ))
        db.create_unique('eventos_file_folders', ['file_id', 'folder_id'])


    def backwards(self, orm):
        # Deleting model 'Instrument'
        db.delete_table('eventos_instrument')

        # Deleting model 'UserProfile'
        db.delete_table('eventos_userprofile')

        # Removing M2M table for field galleries on 'UserProfile'
        db.delete_table('eventos_userprofile_galleries')

        # Removing M2M table for field photos on 'UserProfile'
        db.delete_table('eventos_userprofile_photos')

        # Removing M2M table for field messages on 'UserProfile'
        db.delete_table('eventos_userprofile_messages')

        # Removing M2M table for field instruments on 'UserProfile'
        db.delete_table('eventos_userprofile_instruments')

        # Deleting model 'Category'
        db.delete_table('eventos_category')

        # Removing M2M table for field galleries on 'Category'
        db.delete_table('eventos_category_galleries')

        # Removing M2M table for field photos on 'Category'
        db.delete_table('eventos_category_photos')

        # Removing M2M table for field avatari on 'Category'
        db.delete_table('eventos_category_avatari')

        # Deleting model 'Event'
        db.delete_table('eventos_event')

        # Deleting model 'Assistance'
        db.delete_table('eventos_assistance')

        # Deleting model 'Topic'
        db.delete_table('eventos_topic')

        # Deleting model 'Article'
        db.delete_table('eventos_article')

        # Removing M2M table for field topic on 'Article'
        db.delete_table('eventos_article_topic')

        # Deleting model 'FileType'
        db.delete_table('eventos_filetype')

        # Deleting model 'Folder'
        db.delete_table('eventos_folder')

        # Deleting model 'Song'
        db.delete_table('eventos_song')

        # Removing M2M table for field instruments on 'Song'
        db.delete_table('eventos_song_instruments')

        # Deleting model 'File'
        db.delete_table('eventos_file')

        # Removing M2M table for field ftype on 'File'
        db.delete_table('eventos_file_ftype')

        # Removing M2M table for field songs on 'File'
        db.delete_table('eventos_file_songs')

        # Removing M2M table for field folders on 'File'
        db.delete_table('eventos_file_folders')


    models = {
        'auth.group': {
            'Meta': {'object_name': 'Group'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '80'}),
            'permissions': ('django.db.models.fields.related.ManyToManyField', [], {'to': "orm['auth.Permission']", 'symmetrical': 'False', 'blank': 'True'})
        },
        'auth.permission': {
            'Meta': {'ordering': "('content_type__app_label', 'content_type__model', 'codename')", 'unique_together': "(('content_type', 'codename'),)", 'object_name': 'Permission'},
            'codename': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'content_type': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['contenttypes.ContentType']"}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '50'})
        },
        'auth.user': {
            'Meta': {'object_name': 'User'},
            'date_joined': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now'}),
            'email': ('django.db.models.fields.EmailField', [], {'max_length': '75', 'blank': 'True'}),
            'first_name': ('django.db.models.fields.CharField', [], {'max_length': '30', 'blank': 'True'}),
            'groups': ('django.db.models.fields.related.ManyToManyField', [], {'to': "orm['auth.Group']", 'symmetrical': 'False', 'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'is_active': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'is_staff': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'is_superuser': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'last_login': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now'}),
            'last_name': ('django.db.models.fields.CharField', [], {'max_length': '30', 'blank': 'True'}),
            'password': ('django.db.models.fields.CharField', [], {'max_length': '128'}),
            'user_permissions': ('django.db.models.fields.related.ManyToManyField', [], {'to': "orm['auth.Permission']", 'symmetrical': 'False', 'blank': 'True'}),
            'username': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '30'})
        },
        'contenttypes.contenttype': {
            'Meta': {'ordering': "('name',)", 'unique_together': "(('app_label', 'model'),)", 'object_name': 'ContentType', 'db_table': "'django_content_type'"},
            'app_label': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'model': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '100'})
        },
        'eventos.article': {
            'Meta': {'object_name': 'Article'},
            'attachment': ('django.db.models.fields.files.FileField', [], {'max_length': '100', 'null': 'True', 'blank': 'True'}),
            'description': ('django.db.models.fields.TextField', [], {}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'image': ('eventos.resizeavatar.ResizedImageField', [], {'max_length': '100', 'null': 'True', 'blank': 'True'}),
            'link': ('django.db.models.fields.URLField', [], {'max_length': '500', 'null': 'True', 'blank': 'True'}),
            'title': ('django.db.models.fields.CharField', [], {'max_length': '200'}),
            'topic': ('django.db.models.fields.related.ManyToManyField', [], {'to': "orm['eventos.Topic']", 'symmetrical': 'False'})
        },
        'eventos.assistance': {
            'Meta': {'object_name': 'Assistance'},
            'event': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['eventos.Event']"}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'status': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'user': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['auth.User']"})
        },
        'eventos.category': {
            'Meta': {'object_name': 'Category'},
            'avatari': ('django.db.models.fields.related.ManyToManyField', [], {'blank': 'True', 'related_name': "'avatari'", 'null': 'True', 'symmetrical': 'False', 'to': "orm['photologue.Photo']"}),
            'galleries': ('django.db.models.fields.related.ManyToManyField', [], {'symmetrical': 'False', 'to': "orm['photologue.Gallery']", 'null': 'True', 'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '50'}),
            'photos': ('django.db.models.fields.related.ManyToManyField', [], {'symmetrical': 'False', 'to': "orm['photologue.Photo']", 'null': 'True', 'blank': 'True'}),
            'slug': ('django.db.models.fields.CharField', [], {'max_length': '50'})
        },
        'eventos.event': {
            'Meta': {'object_name': 'Event'},
            'author': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['auth.User']"}),
            'description': ('django.db.models.fields.CharField', [], {'max_length': '1000', 'null': 'True', 'blank': 'True'}),
            'end': ('django.db.models.fields.DateTimeField', [], {'null': 'True', 'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'is_active': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'start': ('django.db.models.fields.DateTimeField', [], {}),
            'title': ('django.db.models.fields.CharField', [], {'max_length': '200'}),
            'users': ('django.db.models.fields.related.ManyToManyField', [], {'related_name': "'events'", 'symmetrical': 'False', 'through': "orm['eventos.Assistance']", 'to': "orm['auth.User']"})
        },
        'eventos.file': {
            'Meta': {'object_name': 'File'},
            'ffile': ('django.db.models.fields.files.FileField', [], {'max_length': '100'}),
            'folders': ('django.db.models.fields.related.ManyToManyField', [], {'symmetrical': 'False', 'to': "orm['eventos.Folder']", 'null': 'True', 'blank': 'True'}),
            'ftype': ('django.db.models.fields.related.ManyToManyField', [], {'to': "orm['eventos.FileType']", 'symmetrical': 'False'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'instrument': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['eventos.Instrument']", 'null': 'True', 'blank': 'True'}),
            'songs': ('django.db.models.fields.related.ManyToManyField', [], {'symmetrical': 'False', 'to': "orm['eventos.Song']", 'null': 'True', 'blank': 'True'}),
            'title': ('django.db.models.fields.CharField', [], {'max_length': '300'})
        },
        'eventos.filetype': {
            'Meta': {'object_name': 'FileType'},
            'extensions': ('django.db.models.fields.CharField', [], {'max_length': '300', 'null': 'True', 'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '300'})
        },
        'eventos.folder': {
            'Meta': {'object_name': 'Folder'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '200'})
        },
        'eventos.instrument': {
            'Meta': {'object_name': 'Instrument'},
            'code': ('django.db.models.fields.CharField', [], {'max_length': '20'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '300'})
        },
        'eventos.song': {
            'Meta': {'object_name': 'Song'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'instruments': ('django.db.models.fields.related.ManyToManyField', [], {'symmetrical': 'False', 'to': "orm['eventos.Instrument']", 'null': 'True', 'blank': 'True'}),
            'title': ('django.db.models.fields.CharField', [], {'max_length': '300'})
        },
        'eventos.topic': {
            'Meta': {'object_name': 'Topic'},
            'description': ('django.db.models.fields.CharField', [], {'max_length': '500', 'null': 'True', 'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '100'})
        },
        'eventos.userprofile': {
            'Meta': {'object_name': 'UserProfile'},
            'address': ('django.db.models.fields.CharField', [], {'max_length': '300', 'null': 'True', 'blank': 'True'}),
            'avatar': ('eventos.resizeavatar.ResizedImageField', [], {'max_length': '100', 'null': 'True', 'blank': 'True'}),
            'description': ('django.db.models.fields.TextField', [], {'null': 'True', 'blank': 'True'}),
            'galleries': ('django.db.models.fields.related.ManyToManyField', [], {'symmetrical': 'False', 'to': "orm['photologue.Gallery']", 'null': 'True', 'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'instruments': ('django.db.models.fields.related.ManyToManyField', [], {'symmetrical': 'False', 'to': "orm['eventos.Instrument']", 'null': 'True', 'blank': 'True'}),
            'messages': ('django.db.models.fields.related.ManyToManyField', [], {'symmetrical': 'False', 'to': "orm['messages.Message']", 'null': 'True', 'blank': 'True'}),
            'photos': ('django.db.models.fields.related.ManyToManyField', [], {'symmetrical': 'False', 'to': "orm['photologue.Photo']", 'null': 'True', 'blank': 'True'}),
            'telephone': ('django.db.models.fields.DecimalField', [], {'null': 'True', 'max_digits': '9', 'decimal_places': '0', 'blank': 'True'}),
            'user': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['auth.User']", 'unique': 'True'})
        },
        'messages.message': {
            'Meta': {'ordering': "['-sent_at']", 'object_name': 'Message'},
            'body': ('django.db.models.fields.TextField', [], {}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'parent_msg': ('django.db.models.fields.related.ForeignKey', [], {'blank': 'True', 'related_name': "'next_messages'", 'null': 'True', 'to': "orm['messages.Message']"}),
            'read_at': ('django.db.models.fields.DateTimeField', [], {'null': 'True', 'blank': 'True'}),
            'recipient': ('django.db.models.fields.related.ForeignKey', [], {'blank': 'True', 'related_name': "'received_messages'", 'null': 'True', 'to': "orm['auth.User']"}),
            'recipient_deleted_at': ('django.db.models.fields.DateTimeField', [], {'null': 'True', 'blank': 'True'}),
            'replied_at': ('django.db.models.fields.DateTimeField', [], {'null': 'True', 'blank': 'True'}),
            'sender': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'sent_messages'", 'to': "orm['auth.User']"}),
            'sender_deleted_at': ('django.db.models.fields.DateTimeField', [], {'null': 'True', 'blank': 'True'}),
            'sent_at': ('django.db.models.fields.DateTimeField', [], {'null': 'True', 'blank': 'True'}),
            'subject': ('django.db.models.fields.CharField', [], {'max_length': '120'})
        },
        'photologue.gallery': {
            'Meta': {'ordering': "['-date_added']", 'object_name': 'Gallery'},
            'date_added': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now'}),
            'description': ('django.db.models.fields.TextField', [], {'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'is_public': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'photos': ('django.db.models.fields.related.ManyToManyField', [], {'blank': 'True', 'related_name': "'galleries'", 'null': 'True', 'symmetrical': 'False', 'to': "orm['photologue.Photo']"}),
            'tags': ('tagging.fields.TagField', [], {}),
            'title': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '50'}),
            'title_slug': ('django.db.models.fields.SlugField', [], {'unique': 'True', 'max_length': '50'})
        },
        'photologue.photo': {
            'Meta': {'ordering': "['-date_added']", 'object_name': 'Photo'},
            'caption': ('django.db.models.fields.TextField', [], {'blank': 'True'}),
            'crop_from': ('django.db.models.fields.CharField', [], {'default': "'center'", 'max_length': '10', 'blank': 'True'}),
            'date_added': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now'}),
            'date_taken': ('django.db.models.fields.DateTimeField', [], {'null': 'True', 'blank': 'True'}),
            'effect': ('django.db.models.fields.related.ForeignKey', [], {'blank': 'True', 'related_name': "'photo_related'", 'null': 'True', 'to': "orm['photologue.PhotoEffect']"}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'image': ('django.db.models.fields.files.ImageField', [], {'max_length': '100'}),
            'is_public': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'tags': ('tagging.fields.TagField', [], {}),
            'title': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '50'}),
            'title_slug': ('django.db.models.fields.SlugField', [], {'unique': 'True', 'max_length': '50'}),
            'view_count': ('django.db.models.fields.PositiveIntegerField', [], {'default': '0'})
        },
        'photologue.photoeffect': {
            'Meta': {'object_name': 'PhotoEffect'},
            'background_color': ('django.db.models.fields.CharField', [], {'default': "'#FFFFFF'", 'max_length': '7'}),
            'brightness': ('django.db.models.fields.FloatField', [], {'default': '1.0'}),
            'color': ('django.db.models.fields.FloatField', [], {'default': '1.0'}),
            'contrast': ('django.db.models.fields.FloatField', [], {'default': '1.0'}),
            'description': ('django.db.models.fields.TextField', [], {'blank': 'True'}),
            'filters': ('django.db.models.fields.CharField', [], {'max_length': '200', 'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '30'}),
            'reflection_size': ('django.db.models.fields.FloatField', [], {'default': '0'}),
            'reflection_strength': ('django.db.models.fields.FloatField', [], {'default': '0.6'}),
            'sharpness': ('django.db.models.fields.FloatField', [], {'default': '1.0'}),
            'transpose_method': ('django.db.models.fields.CharField', [], {'max_length': '15', 'blank': 'True'})
        }
    }

    complete_apps = ['eventos']